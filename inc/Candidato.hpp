#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include <string>

using namespace std;

class Candidato {
    private:
        string estado;
        string cargo;
        string numcandidato;
        string nome;
        string partido;

    public:
        Candidato();
        Candidato(string estado, string cargo, string numcandidato, string nome, string partido);
        ~Candidato();

        void set_estado(string estado);
        string get_estado();

        void set_cargo(string cargo);
        string get_cargo();

        void set_numcandidato(string numcandidato);
        string get_numcandidato();

        void set_nome(string nome);
        string get_nome();

        void set_partido(string partido);
        string get_partido();

        void computa_voto();
};

#endif