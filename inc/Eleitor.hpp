#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include <string>

using namespace std;

class Eleitor {
    private:
        string nome;
        string rg;

    public:
        Eleitor(string nome, string rg);
        ~Eleitor();

        void set_nome(string nome);
        string get_nome();

        void set_rg(string rg);
        string get_rg();

};

#endif