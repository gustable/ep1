#ifndef EXECUTIVO_HPP
#define EXECUTIVO_HPP

#include <string>
#include "Candidato.hpp"

using namespace std;

class Executivo : public Candidato {
    private:
        string vice;
    public:
        Executivo(string estado, string cargo, string numcandidato, string nome, string partido, string vice);
        ~Executivo();

        void set_vice(string vice);
        string get_vice();
};

#endif