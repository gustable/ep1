#ifndef LEITORCSV_HPP
#define LEITORCSV_HPP

#include <string>

using namespace std;

class LeitorCSV {
    private:
        string arquivo;
        char delimitador;

    public:
        LeitorCSV();
        LeitorCSV(string arquivo, char delimitador);
        ~LeitorCSV();

        void set_delimitador(char delimitador);
        char get_delimitador();

        void set_arquivo(string arquivo);
        string get_arquivo();

        void carregar_candidatos(string arquivo, string candidatos[]);
        int contar_linhas(string arquivo);
        void buscar_candidato(string numero);
};

#endif