#ifndef SENADOR_HPP
#define SENADOR_HPP

#include <string>
#include "Candidato.hpp"

using namespace std;

class Senador : public Candidato{
    private:
        string primeirosuplente;
        string segundosuplente;
    public:
        Senador(string estado, string numcandidato, string nome, string partido, string primeirosuplente, string segundosuplente);
        ~Senador();

        void set_primeirosuplente(string primeirosuplente);
        string get_primeirosuplente();

        void set_segundosuplente(string segundosuplente);
        string get_segundosuplente();
};

#endif