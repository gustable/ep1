#ifndef URNA_HPP
#define URNA_HPP

#include <string>

using namespace std;

class Urna {
    public:
        Urna();

        void inserir_codigo(int codigo);
        void cancelar_ultimo();
        void confirmar();
        void branco();
        void relatorio();
        void lista_eleitores(int num_eleitores);

        void iniciar(int num_eleitores);
};

#endif