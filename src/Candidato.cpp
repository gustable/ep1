#include "Candidato.hpp"
#include <iostream>
#include <string>

using namespace std;

Candidato::Candidato(){}

Candidato::Candidato(string estado, string cargo, string numcandidato, string nome, string partido) {
    set_estado(estado);
    set_cargo(cargo);
    set_numcandidato(numcandidato);
    set_nome(nome);
    set_partido(partido);
}

Candidato::~Candidato(){}

void Candidato::set_estado(string estado) {this->estado = estado;}
string Candidato::get_estado(){return estado;}

void Candidato::set_cargo(string cargo) {this->cargo = cargo;}
string Candidato::get_cargo() {return cargo;}

void Candidato::set_numcandidato(string numcandidato) {this->numcandidato = numcandidato;}
string Candidato::get_numcandidato() {return numcandidato;}

void Candidato::set_nome(string nome) {this->nome = nome;}
string Candidato::get_nome() {return nome;}

void Candidato::set_partido(string partido) {this->partido = partido;}
string Candidato::get_partido() {return partido;}

/**
 * Registra quem votou no candidato, incluindo seu nome e RG 
 */
void Candidato::computa_voto() {
    //implementar aqui
}