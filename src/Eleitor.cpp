#include "Eleitor.hpp"
#include <iostream>
#include <string>

using namespace std;

//construtores e destrutores
Eleitor::Eleitor(string nome, string rg) {
    set_nome(nome);
    set_rg(rg);
}

void Eleitor::set_nome(string nome) {this->nome = nome;}
string Eleitor::get_nome(){return nome;}

void Eleitor::set_rg(string rg) {this->rg = rg;}
string Eleitor::get_rg() {return rg;}