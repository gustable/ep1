#include "Executivo.hpp"
#include <iostream>
#include <string>

using namespace std;

Executivo::Executivo(string estado, string cargo, string numcandidato, string nome, string partido, string vice) {
    set_estado(estado);
    set_cargo(cargo);
    set_numcandidato(numcandidato);
    set_nome(nome);
    set_partido(partido);
    set_vice(vice);
}

void Executivo::set_vice(string vice) {this->vice = vice;}
string Executivo::get_vice(){return vice;}
