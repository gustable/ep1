#include "LeitorCSV.hpp"
#include "Candidato.hpp"
#include "Executivo.hpp"
#include "Senador.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

LeitorCSV::LeitorCSV() {set_delimitador(';');}
LeitorCSV::LeitorCSV(string arquivo, char delimitador) {
    set_delimitador(delimitador);
    set_arquivo(arquivo);
}

void LeitorCSV::set_delimitador(char delimitador) {this->delimitador = delimitador;}
char LeitorCSV::get_delimitador() {return delimitador;}

void LeitorCSV::set_arquivo(string arquivo) {this->arquivo = arquivo;}
string LeitorCSV::get_arquivo() {return arquivo;}

/**
 * Parâmetro "string arquivo" igual ao endereço do arquivo .csv 
 */
void LeitorCSV::carregar_candidatos(string arquivo, string candidatos[]) {
    ifstream planilha;
    LeitorCSV * leitor = new LeitorCSV();
    char delimitador = (char)leitor->get_delimitador();

    planilha.open(arquivo.c_str(),ios_base::in);
    if(!planilha.is_open()) cout << "Não foi possível acessar seu arquivo" << endl;

    string linha;
    getline(planilha, linha, '\n');

    int num_linhas = leitor->contar_linhas(arquivo);
    string candidatos_provisorio[num_linhas][58];
    int i,j;

    for(i = 0; i < num_linhas-1; i++) {
        for(j = 0; j < 57; j++) {
            getline(planilha,candidatos_provisorio[i][j],delimitador);
            // cout << candidatos[i][j] << endl;
        }
        getline(planilha,candidatos_provisorio[i][j],'\n');
        // cout << candidatos[i][j] << endl;
    }

    vector < string > presidente_governador;
    vector < int > posicao_i_presidente_governador;

    vector < string > vice;
    vector < int > posicao_i_vice;

    vector < string > senador;
    vector < int > posicao_i_senador;

    vector < string > suplente1;
    vector < int > posicao_i_suplente1;

    vector < string > suplente2;
    vector < int > posicao_i_suplente2;

    vector < string > deputado;
    vector < int > posicao_i_deputado;


    for(i = 0; i < num_linhas-1; i++) {
        for(j = 0; j < 57; j++) {
            if(j == 14) {
                if(candidatos_provisorio[i][j] == "PRESIDENTE" || candidatos_provisorio[i][j] == "GOVERNADOR") {
                    string numcandidato = candidatos_provisorio[i][j+2];
                    presidente_governador.push_back(numcandidato);
                    posicao_i_presidente_governador.push_back(i);

                } else if(candidatos_provisorio[i][j] == "VICE-PRESIDENTE" || candidatos_provisorio[i][j] == "VICE-GOVERNADOR" ) {
                    string numcandidato = candidatos_provisorio[i][j+2];
                    vice.push_back(numcandidato);
                    posicao_i_vice.push_back(i);

                } else if(candidatos_provisorio[i][j] == "SENADOR") {
                    string numcandidato = candidatos_provisorio[i][j+2];
                    senador.push_back(numcandidato);
                    posicao_i_senador.push_back(i);

                } else if(candidatos_provisorio[i][j] == "1º SUPLENTE") {
                    string numcandidato = candidatos_provisorio[i][j+2];
                    suplente1.push_back(numcandidato);
                    posicao_i_suplente1.push_back(i);

                } else if(candidatos_provisorio[i][j] == "2º SUPLENTE") {
                    string numcandidato = candidatos_provisorio[i][j+2];
                    suplente2.push_back(numcandidato);
                    posicao_i_suplente2.push_back(i);

                } else if(candidatos_provisorio[i][j] == "DEPUTADO DISTRITAL" || candidatos_provisorio[i][j] == "DEPUTADO FEDERAL") {
                    string numcandidato = candidatos_provisorio[i][j+2];
                    deputado.push_back(numcandidato);
                    posicao_i_deputado.push_back(i);

                }
            }
        }
    }
}

// string estado = candidatos_provisorio[i][j-3];
// string cargo  = candidatos_provisorio[i][j];
// string nome = candidatos_provisorio[i][j+3];
// string partido = candidatos_provisorio[i][j+14];

/**
 * Conta quantas linhas possui o arquivo .csv
 */
int LeitorCSV::contar_linhas(string arquivo) {
    ifstream planilha;
    LeitorCSV * leitor = new LeitorCSV();

    planilha.open(arquivo.c_str(),ios_base::in);

    if(!planilha.is_open()) cout << "Não foi possível acessar seu arquivo" << endl;

    string linha;
    int num_linhas = 0;
    
    while(getline(planilha, linha)) num_linhas++;
    
    planilha.close();
    return num_linhas;
}

void LeitorCSV::buscar_candidato(string numero) {
    //implementar
}