#include "Senador.hpp"
#include <iostream>
#include <string>

using namespace std;

Senador::Senador(string estado, string numcandidato, string nome, string partido, string primeirosuplente, string segundosuplente) {
    set_estado(estado);
    set_numcandidato(numcandidato);
    set_nome(nome);
    set_partido(partido);
    set_primeirosuplente(primeirosuplente);
    set_segundosuplente(segundosuplente);
}

void Senador::set_primeirosuplente(string primeirosuplente) {this->primeirosuplente = primeirosuplente;}
string Senador::get_primeirosuplente(){return primeirosuplente;}

void Senador::set_segundosuplente(string segundosuplente) {this->segundosuplente = segundosuplente;}
string Senador::get_segundosuplente() {return segundosuplente;}