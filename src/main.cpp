#include <iostream>
#include "leitorCSV.hpp"
#include "Urna.hpp"

using namespace std;

int main() {
    LeitorCSV * leitor = new LeitorCSV();

    string arquivo = "../data/consulta_cand_2018_BR.csv";

    int eleitores;
    int tamanho = leitor->contar_linhas(arquivo);
    string candidatos[tamanho];

    leitor->carregar_candidatos(arquivo, candidatos);

    Urna * urna = new Urna();
    cout << "Quantos eleitores usaram essa urna?: " ;
    cin >> eleitores;
    urna->iniciar(eleitores);

    return 0;
}
